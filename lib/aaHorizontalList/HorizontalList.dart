import 'package:flutter/material.dart';

class HList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: new ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Category(
            imageCaption: 'Shirt',
            imageLocation: 'images/cats/tshirt.png',
          ),
          Category(
            imageCaption: 'Shoe',
            imageLocation: 'images/cats/shoe.png',
          ),
          Category(
            imageCaption: 'Jeans',
            imageLocation: 'images/cats/jeans.png',
          ),
          Category(
            imageCaption: 'Informal',
            imageLocation: 'images/cats/informal.png',
          ),
          Category(
            imageCaption: 'Formal',
            imageLocation: 'images/cats/formal.png',
          ),
          Category(
            imageCaption: 'Dress',
            imageLocation: 'images/cats/dress.png',
          ),
        ],
      ),
    );
  }
}

class Category extends StatelessWidget {
  final String imageLocation, imageCaption;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(2.0),
      child: new InkWell(
        onTap: () {},
        child: new Container(
          width: 100,
          child: new ListTile(
            title: new Image.asset(
              imageLocation,
              width: 100,
              height: 80,
            ),
            subtitle: new Container(
              alignment: Alignment.topCenter,
              child: new Text(imageCaption),
            ),
          ),
        ),
      ),
    );
  }

  Category({this.imageLocation, this.imageCaption});
}
