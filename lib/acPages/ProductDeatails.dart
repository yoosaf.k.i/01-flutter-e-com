import 'package:flutter/material.dart';

class ProductDetails extends StatefulWidget {
  @override
  final prod_name;
  final prod_pricture;
  final prod_old_price;
  final prod_price;

  ProductDetails({
    this.prod_name,
    this.prod_pricture,
    this.prod_old_price,
    this.prod_price,
  });
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  void _showMaterialDialog(
    BuildContext context,
    String title,
  ) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(title),
            content: Text("Choose the ${title}"),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    _dismissDialog(context);
                  },
                  child: Text('Close')),
              FlatButton(
                onPressed: () {
                  //print('HelloWorld!');
                  _dismissDialog(context);
                },
                child: Text('select'),
              )
            ],
          );
        });
  }

  void _dismissDialog(BuildContext context) {
    Navigator.of(context).pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        elevation: 0.0,
        backgroundColor: Colors.deepOrange,
        title: new Text(widget.prod_name),
        actions: <Widget>[
          new IconButton(
              icon: new Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () {}),
          new IconButton(
              icon: new Icon(
                Icons.shopping_cart,
                color: Colors.white,
              ),
              onPressed: () {})
        ],
      ),
      body: new ListView(
        children: <Widget>[
          new Container(
            height: 300,
            child: new GridTile(
              child: new Container(
                child: Image.asset(widget.prod_pricture),
                color: Colors.white,
              ),
              footer: Container(
                height: 60,
                color: Colors.white70,
                child: ListTile(
                    leading: Text(
                      widget.prod_name,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    title: new Row(
                      children: <Widget>[
                        new Expanded(
                          child: Text(
                            "\$${widget.prod_price}",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.red, fontWeight: FontWeight.bold),
                          ),
                        ),
                        new Expanded(
                          child: Text(
                            "\$${widget.prod_old_price}",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                decoration: TextDecoration.lineThrough,
                                color: Colors.grey,
                                fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    )),
              ),
            ),
          ),
          new Row(
            children: <Widget>[
              new Expanded(
                  child: new MaterialButton(
                onPressed: () {
                  _showMaterialDialog(context, "Size");
                },
                color: Colors.white,
                textColor: Colors.grey,
                elevation: 0.2,
                child: new Row(
                  children: <Widget>[
                    new Expanded(
                      child: new Text("Size"),
                    ),
                    new Expanded(
                      child: new Icon(Icons.arrow_drop_down),
                    ),
                  ],
                ),
              )),
              new Expanded(
                  child: new MaterialButton(
                onPressed: () {
                  _showMaterialDialog(context, "Color");
                },
                color: Colors.white,
                textColor: Colors.grey,
                elevation: 0.2,
                child: new Row(
                  children: <Widget>[
                    new Expanded(
                      child: new Text("Color"),
                    ),
                    new Expanded(
                      child: new Icon(Icons.arrow_drop_down),
                    ),
                  ],
                ),
              )),
              new Expanded(
                  child: new MaterialButton(
                onPressed: () {
                  _showMaterialDialog(context, "Qnty");
                },
                color: Colors.white,
                textColor: Colors.grey,
                elevation: 0.2,
                child: new Row(
                  children: <Widget>[
                    new Expanded(
                      child: new Text("Qnty"),
                    ),
                    new Expanded(
                      child: new Icon(Icons.arrow_drop_down),
                    ),
                  ],
                ),
              ))
            ],
          ),
          new Row(
            children: <Widget>[
              new Expanded(
                  child: new MaterialButton(
                onPressed: () {},
                color: Colors.red,
                textColor: Colors.white,
                elevation: 0.2,
                child: new Row(
                  children: <Widget>[
                    new Expanded(
                      child: new Text(
                        "Size",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              )),
              new IconButton(
                  icon: new Icon(
                    Icons.add_shopping_cart,
                    color: Colors.red,
                  ),
                  onPressed: () {}),
              new IconButton(
                  icon: new Icon(
                    Icons.favorite_border,
                    color: Colors.red,
                  ),
                  onPressed: () {}),
            ],
          ),
          new Divider(),
          new ListTile(
            title: new Text("Product Details"),
            subtitle: new Text(
                "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)."),
          ),
          new Divider(),
          new Row(
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.fromLTRB(12, 5, 5, 5),
                child: new Text("Product Name ",
                    style: new TextStyle(color: Colors.grey)),
              ),
              Expanded(
                child: new Padding(
                    padding: EdgeInsets.all(10),
                    child: new Text(widget.prod_name,
                        textAlign: TextAlign.right,
                        style: new TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold))),
              ),
            ],
          ),
          new Row(
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.fromLTRB(12, 5, 5, 5),
                child: new Text(
                  "Product Brand ",
                  style: new TextStyle(
                    color: Colors.grey,
                  ),
                ),
              ),
              Expanded(
                child: new Padding(
                    padding: EdgeInsets.all(10),
                    child: new Text("BrnadX ",
                        textAlign: TextAlign.right,
                        style: new TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold))),
              ),
            ],
          ),
          new Row(
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.fromLTRB(12, 5, 5, 5),
                child: new Text("Product Condition ",
                    style: new TextStyle(color: Colors.grey)),
              ),
              Expanded(
                child: new Padding(
                    padding: EdgeInsets.all(10),
                    child: new Text(
                      "New ",
                      textAlign: TextAlign.right,
                      style: new TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    )),
              ),
            ],
          ),
        ],
      ),
    );
    ;
  }
}
/*

 */
