import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ecom_app/aaHorizontalList/HorizontalList.dart';
import 'package:flutter_ecom_app/abGridList/GridListType1.dart';

void main() {
  runApp(
    new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new HomePage(),
    ),
  );
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    Widget img_crl = new Container(
      height: 300.0,
      child: new Carousel(
        boxFit: BoxFit.cover,
        images: [
          AssetImage('images/m1.jpeg'),
          AssetImage('images/w1.jpeg'),
          AssetImage('images/w3.jpeg'),
          AssetImage('images/w4.jpeg'),
          AssetImage('images/c1.jpg'),
          AssetImage('images/IMG_1266.JPG'),
        ],
        autoplay: false,
        animationCurve: Curves.fastLinearToSlowEaseIn,
        dotSize: 8.0,
        dotColor: Colors.white,
        indicatorBgPadding: 8.0,
        animationDuration: new Duration(milliseconds: 1000),
      ),
    );
    return new Scaffold(
      appBar: new AppBar(
        elevation: 0.0,
        backgroundColor: Colors.deepOrange,
        title: new Text("My Shop App"),
        actions: <Widget>[
          new IconButton(
              icon: new Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () {}),
          new IconButton(
              icon: new Icon(
                Icons.shopping_cart,
                color: Colors.white,
              ),
              onPressed: () {})
        ],
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text("Krishanan"),
              accountEmail: new Text("a@a.com"),
              currentAccountPicture: new GestureDetector(
                child: new CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: new Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                ),
              ),
              decoration:
                  new BoxDecoration(color: Colors.red, border: Border.all()),
            ),
            InkWell(
              onTap: () {},
              child: new ListTile(
                title: new Text("Home Page"),
                leading: new Icon(Icons.home),
              ),
            ),
            InkWell(
              onTap: () {},
              child: new ListTile(
                title: new Text("My Account"),
                leading: new Icon(Icons.person),
              ),
            ),
            InkWell(
              onTap: () {},
              child: new ListTile(
                title: new Text("My Orders"),
                leading: new Icon(Icons.shopping_basket),
              ),
            ),
            InkWell(
              onTap: () {},
              child: new ListTile(
                title: new Text("Categories"),
                leading: new Icon(Icons.dashboard),
              ),
            ),
            InkWell(
              onTap: () {},
              child: new ListTile(
                title: new Text("Favourite"),
                leading: new Icon(Icons.favorite),
              ),
            ),
            new Divider(),
            InkWell(
              onTap: () {},
              child: new ListTile(
                title: new Text("Settings"),
                leading: new Icon(
                  Icons.settings,
                  color: Colors.lightBlueAccent,
                ),
              ),
            ),
            InkWell(
              onTap: () {},
              child: new ListTile(
                title: new Text("About"),
                leading: new Icon(
                  Icons.help,
                  color: Colors.deepOrangeAccent,
                ),
              ),
            ),
          ],
        ),
      ),
      body: new ListView(
        children: <Widget>[
          img_crl,
          new Padding(
            padding: new EdgeInsets.all(8.0),
            child: new Text("Categories"),
          ),
          new HList(),
          new Padding(
            padding: new EdgeInsets.all(20.0),
            child: new Text("Product"),
          ),
          Container(
            height: 320,
            child: ProductGridList(),
          )
        ],
      ),
    );
  }
}
